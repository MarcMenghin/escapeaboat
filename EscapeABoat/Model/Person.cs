﻿using System;
using System.Collections.Generic;

namespace EscapeABoat.Model
{
    internal sealed class Person
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public DateTime Birthdate { get; set; }
        public string Bio { get; set; }
        public List<Log> Logs { get; set; }
    }
}
