﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EscapeABoat.Model
{
    internal sealed class Log
    {

        public int Day { get; set; }
        public string Text { get; set; }

        public DateTime Date => Program.VoyageStart.AddDays(Day);
    }
}
