﻿using System;
using System.Collections.Generic;

namespace EscapeABoat.Core
{
    internal sealed class ConsoleService
    {
        public ConsoleBase Current { get; private set; }

        private readonly Stack<ConsoleBase> activeConsoles;

        public ConsoleService()
        {
            activeConsoles = new Stack<ConsoleBase>();
        }

        public void OpenConsole(ConsoleBase console)
        {
            activeConsoles.Push(console);

            SetupConsole(console);
        }

        public void CloseConsole()
        {
            var oldConsole = activeConsoles.Pop();

            if (activeConsoles.Count > 0)
            {
                SetupConsole(activeConsoles.Peek());
            }
            else
            {
                SetupConsole(null);
            }
        }

        private void SetupConsole(ConsoleBase console)
        {
            Current = console;
            if (console != null)
            {
                Console.Clear();
                console.WriteHeader();
            }
        }
    }
}
