﻿using System.Linq;

namespace EscapeABoat.Core
{
    internal sealed class CommandParser
    {
        public string[] Parse(string command)
        {
            return command.Split(" ").Select(s => s.Trim()).ToArray();
        }
    }
}
