﻿using System.Collections.Generic;
using System.Linq;
using EscapeABoat.Helpers;
using NLog;

namespace EscapeABoat.Core
{
    internal abstract class ConsoleBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        private readonly CommandParser parser;


        protected readonly Dictionary<string, ICommandHandler> commands;

        public abstract void WriteHeader();
        public abstract string GetInput();

        public int MaxHistoryEntries { get; set; }
        public Queue<string> History { get; private set; }

        protected ConsoleBase()
        {
            parser = new CommandParser();
            History = new Queue<string>();
            this.commands = new Dictionary<string, ICommandHandler>();
            MaxHistoryEntries = 30;
        }

        public void ExecuteCommand(string commandEntry)
        {
            var commandList = parser.Parse(commandEntry);

            ICommandHandler currentCommand;

            if (commands.TryGetValue(commandList[0].ToLower(), out currentCommand))
            {
                currentCommand.Execute(commandList.Skip(1).Take(commandList.Length - 1).ToArray());

                AddHistory(commandEntry);
            }
            else
            {
                ConHelper.OutLineHint("Unknown command.");
            }
        }

        protected void RegisterCommand(ICommandHandler handler)
        {
            commands.Add(handler.Handle.ToLower(), handler);
        }

        protected void AddHistory(string entry)
        {
            History.Enqueue(entry);

            //cleanup
            var cleanupZone = (int)(MaxHistoryEntries * 1.1);

            if (History.Count > cleanupZone)
            {
                while (History.Count > MaxHistoryEntries)
                {
                    History.Dequeue();
                }
            }
        }
    }
}
