﻿using System;
using System.Linq;
using EscapeABoat.Core;
using EscapeABoat.Helpers;

namespace EscapeABoat.Commands
{
    internal sealed class HistoryCommand : ICommandHandler
    {
        private readonly ConsoleBase _console;
        public string Handle => "his";
        public string[] Help => new[] { "Shows history of entered commands." };
        public ValueTuple<string, string>[] Usage => new[] { new ValueTuple<string, string>("his", null) };
        public bool Secret => false;

        public HistoryCommand(ConsoleBase console)
        {
            _console = console;
        }

        public void Execute(string[] data)
        {
            ConHelper.OutLine("Last entered commands are:");
            foreach (var history in _console.History.Reverse())
            {
                ConHelper.OutLineHint($"  {history}");
            }
        }
    }
}
