﻿using System;
using EscapeABoat.Core;

namespace EscapeABoat.Commands
{
    internal sealed class ClearCommand : ICommandHandler
    {
        private readonly ConsoleBase _console;

        public string Handle => "clear";
        public string[] Help => new[] { "Clears the terminal." };
        public ValueTuple<string, string>[] Usage => new[] { new ValueTuple<string, string>("clear", null) };
        public bool Secret => false;

        public ClearCommand(ConsoleBase console)
        {
            _console = console;
        }
        public void Execute(string[] data)
        {
            Console.Clear();
            _console.WriteHeader();
        }
    }
}
