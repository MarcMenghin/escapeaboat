﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EscapeABoat.Core;
using EscapeABoat.Helpers;
using EscapeABoat.Model;
using EscapeABoat.Repositories;

namespace EscapeABoat.Commands
{
    public sealed class PassangerCommand : ICommandHandler
    {
        public string Handle => "pl";
        public bool Secret => false;

        public string[] Help => new[]
        {
            "Lists passangers and associated information.",
        };
        public ValueTuple<string, string>[] Usage => new[]
        {
            new ValueTuple<string, string>("pl", "to list all passangers"),
            new ValueTuple<string, string>("pl [number]", "to show detailed information on a passanger")

        };

        private Dictionary<int, Person> persons;


        public PassangerCommand()
        {
            LoadData();
        }

        public void Execute(string[] data)
        {
            if (data.Length <= 0)
            {
                ConHelper.OutLine("List of passangers:");
                foreach (var person in persons.OrderBy(pair => pair.Key))
                {
                    ConHelper.OutLine($"  Nr.{person.Value.Number} {person.Value.Name}");
                }
            }
            else
            {

                int personId;
                if (!int.TryParse(data[0], out personId))
                {
                    ConHelper.OutLine("Couldn't read passanger number.");
                }
                else
                {
                    Person person;
                    if (persons.TryGetValue(personId, out person))
                    {
                        ConHelper.OutLine($"Number: {person.Number}");
                        ConHelper.OutLine($"Name: {person.Name}");
                        ConHelper.OutLine($"Birthdate: {person.Birthdate.ToShortDateString()}");
                        ConHelper.OutLine($"Biography: {person.Bio}");
                        ConHelper.OutLine($"Log entries: {person.Logs.Count}");
                    }
                    else
                    {
                        ConHelper.OutLine($"No passanger with number {personId}");
                    }
                }
            }

        }

        private void LoadData()
        {
            persons = PersonRepository.GetPersons();
        }
    }
}
