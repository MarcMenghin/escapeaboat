﻿using System;
using System.Linq;
using EscapeABoat.Core;
using EscapeABoat.Helpers;
using EscapeABoat.Repositories;

namespace EscapeABoat.Commands
{
    public sealed class EnergyManualCommand : ICommandHandler
    {
        public string Handle => "man";
        public string[] Help => new[] { "Energy System Manual." };
        public ValueTuple<string, string>[] Usage => new[]
        {
            new ValueTuple<string, string>("man", "to list pages."),
            new ValueTuple<string, string>("man [page number]", "to show a page")
        };

        public bool Secret => false;
        public void Execute(string[] data)
        {
            var manuals = EnergyManualRepository.GetManuals();

            if (data.Length <= 0)
            {
                ConHelper.OutLine("Interresting manual pages:");
                foreach (var manual in manuals.OrderBy(pair => pair.Value.Page))
                {
                    ConHelper.OutLine($"  p.{manual.Key} .. {manual.Value.Headline}");
                }
            }
            else if (data.Length == 1)
            {
                int pageNr;

                if (int.TryParse(data[0], out pageNr))
                {
                    if (!manuals.ContainsKey(pageNr))
                    {
                        ConHelper.OutLine("Unknown Page.");
                        return;
                    }
                    var page = manuals[pageNr];
                    ConHelper.OutLineImportant(page.Headline);
                    foreach (var text in page.Text)
                    {
                        ConHelper.OutLine(text);
                    }
                }
                else
                {
                    ConHelper.OutLine("Not a page number.");
                }
            }
            else
            {
                ConHelper.OutLine("Wrong Command usage.");
            }
        }
    }
}
