﻿using System;
using EscapeABoat.Consoles;
using EscapeABoat.Core;

namespace EscapeABoat.Commands
{
    public sealed class OpenCommand : ICommandHandler
    {
        public string Handle => "open";
        public string[] Help => new[] { "Opens a connection to the terminal" };
        public ValueTuple<string, string>[] Usage => new[] { new ValueTuple<string, string>("open [systemname] -u [user] -p [password]", null) };
        public bool Secret => true;

        public void Execute(string[] data)
        {
            if (data.Length == 5
                && data[0] == "EnergySystem"
                && data[1] == "-u"
                && data[2] == "root"
                && data[3] == "-p"
                && data[4] == "root")
            {
                Program.consoleService.OpenConsole(new EnergyConsole());
            }
        }
    }
}
