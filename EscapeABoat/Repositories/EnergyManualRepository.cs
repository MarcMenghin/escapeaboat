﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using EscapeABoat.Model;
using Newtonsoft.Json;
using NLog;

namespace EscapeABoat.Repositories
{
    internal static class EnergyManualRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static Dictionary<int, Manual> data;

        public static Dictionary<int, Manual> GetManuals()
        {
            EnsureDataLoaded();

            return data;
        }

        private static void EnsureDataLoaded()
        {

            if (data != null)
            {
                return;
            }

            LoadData();
            EnrichData();
        }

        private static void LoadData()
        {
            var dataDirectoryRoot = Program.DataLocation.GetDirectories("energymanual").FirstOrDefault();
            if (dataDirectoryRoot == null)
            {
                Logger.Warn("Missing Manuals directory.");
                data = new Dictionary<int, Manual>();
                return;
            }

            Logger.Info($" Manuals directory: {dataDirectoryRoot.FullName}");
            var dataFiles = dataDirectoryRoot.GetFiles();

            data = new Dictionary<int, Manual>(dataFiles.Length);

            foreach (var dataFile in dataFiles)
            {
                var content = File.ReadAllText(dataFile.FullName);

                var fileData = JsonConvert.DeserializeObject<Manual>(content);


                data.Add(fileData.Page, fileData);


            }
        }

        private static void EnrichData()
        {
        }
    }
}
