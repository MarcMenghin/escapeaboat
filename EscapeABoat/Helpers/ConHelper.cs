﻿using System;

namespace EscapeABoat.Helpers
{
    internal static class ConHelper
    {
        public static void CmdFormat()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Black;
        }

        public static string NextCommand(string consoleName)
        {
            Console.WriteLine("");

            CmdFormat();
            Console.Write($"<{consoleName}>: ");

            Console.ForegroundColor = ConsoleColor.DarkYellow;

            return Console.ReadLine().Trim();
        }

        public static void OutLine(string output)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(output);
        }

        public static void Out(string output)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(output);
        }

        public static void OutLineImportant(string output)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(output);
        }

        public static void OutImportant(string output)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(output);
        }

        public static void OutLineUrgent(string output)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(output);
        }

        public static void OutUrgent(string output)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(output);
        }

        public static void OutLineHint(string output)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(output);
        }

        public static void OutHint(string output)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(output);
        }
    }
}
