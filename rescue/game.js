const ERROR_MESSAGES = [
    "Sadly that is wrong. Maybe you missed something along the way?",
    "Wrong... It probably is the Captains fault!",
    "Not correct. Perhaps there is a clue that you have missed?",
    "Incorrect. Maybe the Captain has some vital information for you?"
]

let NAVIGATION = [
    //{ name: 'Deck', value: 'level1.json' }
]

let levelCounter = 1;
let goodEnding = true;

let globalMorseCode = [];
let tempMorseCode = [];
let globalInterval = "";



function startGame() {
    document.getElementById("intro").innerHTML = "";
    loadLevel("level" + levelCounter + ".json")
}

function createNavigation() {
    let nav = document.createElement("div");

    for(let level of NAVIGATION) {
        let button = document.createElement("input");
        button.type = "button";
        button.value = level.name;
        button.addEventListener("click", function() {
            loadLevel(level.value);
        });
        
        nav.appendChild(button);
        nav.appendChild(document.createElement("br"));
    }

    return nav;
}

function renderLevel(result) {
    let currentLevel = "";
    let notInNav = true;
    let game = document.getElementById("game");

    // Add current level to navigation
    for(let item of NAVIGATION) {
        if(item.name == result.name) {
            notInNav = false;
        }
    }

    if(notInNav) {
        NAVIGATION.push({ name: result.name, value: result.path });
    }

    //Load navigation
    document.getElementById("navigation").innerHTML = "<h3>Map</h3>";
    document.getElementById("navigation").appendChild(createNavigation());

    //Load the image
    let img = document.createElement("img");
    img.src = result.image

    clearElements();

    game.innerHTML = "";
    game.appendChild(img);

    //Load the intro text
    document.getElementById("intro").innerHTML = result.intro;

    if(levelCounter == 5 && !goodEnding) {
        document.getElementById("intro").innerHTML += "<br><br>The captain stares at the wound you got from messing with the monster. 'It can't be a good thing that you got hit by the monster! You have to get that checked out immediatly once we get on land'<br><br>Back on the shore you immediatly visit the nearest hospital and tell them what happened. They look scared.<br><br>A few days later your lab results come back. Turns out the research ship was working on new bio weapons. The doctors only give you a few weeks.<br><br>You managed to save the Captain but the decisions you made along the way cost you your life..."
    }

    //Create clickable areas
    for(let poi of result.poi) {
        let el = createElement(poi);

        el.addEventListener("click", () => {
            // clear fields when changing POI
            clearElements();

            let tooltip = document.getElementById("tooltip") 
            tooltip.innerHTML = poi.tooltip

            if(poi.eventType) {
                if(poi.eventType == "numberInput") {
                    createNumberInputs();
                } else if(poi.eventType == "switchbox") {
                    createSwitchbox();
                } else if(poi.eventType == "choiceSelection") {
                    createChoices(poi.choices);
                }
                if(poi.eventSolution) {
                    tooltip.innerHTML += '<input type="hidden" value="' + poi.eventSolution + '" id="eventSolution">';
                }
            }

            if(poi.morseCode) {
                createMorseCodeElement(poi.morseCode);
            }
        });

        game.appendChild(el);
    }
}

function loadLevel(level) {
    let jsonRequest = $.getJSON(level);
    jsonRequest.done(function(result) {
        renderLevel(result);
    });
}

function clearElements() {
    document.getElementById("tooltip").innerHTML = "";
    document.getElementById("messages").innerHTML = "";
    document.getElementById("controls").innerHTML = "";
    document.getElementById("morse").innerHTML = "";
}

function createMorseCodeElement(morseCode) {
    let imgContainer = document.createElement("div");
    imgContainer.style.position = "relative";
    imgContainer.style.cssFloat = "right";
    imgContainer.style.marginBottom = "75px";

    let img = document.createElement("img");
    img.src = "img/morseCode.jpg";

    let light = document.createElement("div");
    light.style.width = "70px";
    light.style.height = "70px";
    light.style.borderRadius = "50%";
    light.style.backgroundColor = "red";
    light.style.position = "absolute";
    light.style.left = "400px";
    light.style.top = "50px";
    light.id = "morseLight";

    let morseHelper = document.createElement("div");
    morseHelper.position = "absolute";
    morseHelper.style.left = "400px";
    morseHelper.style.top = "140px";
    morseHelper.id = "morseHelper";

    let startButton = document.createElement("input");
    startButton.type = "button";
    startButton.value = "play";
    startButton.addEventListener("click", () => {
        showCode(light);
        startButton.disabled = "true";
    });

    imgContainer.appendChild(img);
    imgContainer.appendChild(document.createElement("br"));
    imgContainer.appendChild(startButton);
    imgContainer.appendChild(light);
    imgContainer.appendChild(morseHelper);

    document.getElementById("morse").appendChild(imgContainer);
    globalMorseCode = morseCode;
    tempMorseCode = globalMorseCode.slice();
}

function showCode(light) {
    globalInterval = window.setInterval(function() {
        let morseHelper = document.getElementById("morseHelper");

        if(tempMorseCode[0] == 1) {
            light.style.display = "block";
            light.style.backgroundColor = "white";

            if(morseHelper.innerHTML == "") {
                if(tempMorseCode[1] == 1) {
                    morseHelper.innerHTML = "long";
                }
                else {
                    morseHelper.innerHTML = "short";
                }
            }
        }
        else if(tempMorseCode[0] == 0) {
            light.style.display = "block";
            light.style.backgroundColor = "black";
            morseHelper.innerHTML = "";
        }
        else if(tempMorseCode[0] == 2) {
            light.style.backgroundColor = "red";
            morseHelper.innerHTML = "";
        }

        tempMorseCode.splice(0, 1);

        if(tempMorseCode.length == 0) {
            tempMorseCode = globalMorseCode.slice();
        }
    }, 500);
}

function checkSolution(value, delay = 3000) {
    let solution = document.getElementById("eventSolution").value;

    if(value == solution) {
        document.getElementById("messages").innerHTML = "correct!<br><br>The next stage begins in " + (delay / 1000) + " seconds.";

        //ToDo: load next level after like 3 seconds or something (setInterval)
        setTimeout(() => {
            levelCounter++;
            loadLevel("level" + levelCounter + ".json")
        }, delay);
    }
    else {
        // randomize error messages
        if(delay == 10000) {
            goodEnding = false;
        }

        let rng = Math.floor(Math.random() * (ERROR_MESSAGES.length - 1));
        document.getElementById("messages").innerHTML = ERROR_MESSAGES[rng];
    }
}

function createElement(poi) {
    let el = document.createElement("div");
    el.style.position = "absolute";
    el.style.top = poi.y + "px";
    el.style.left = poi.x + "px";
    el.style.width = poi.width + "px";
    el.style.height = poi.height + "px";
    el.style.cursor = "pointer";
    //el.style.backgroundColor = "lightblue";

    return el;
}

function createSwitchbox() {
    let container = document.createElement("div");
    container.style.position = "relative";
    container.style.marginBottom = "25px";

    let img = document.createElement("img");
    img.src = "img/switchbox.jpg";
    img.style.width = "100%";
    img.style.height = "auto";

    container.appendChild(img);

    let button = document.createElement("input");
    button.type = "button";
    button.value = "Submit solution";
    button.addEventListener("click", function() {
        if(checkSwitchSolution) {
            document.getElementById("messages").innerHTML = "correct!<br><br>The next stage begins in 3 seconds.";
    
            setTimeout(() => {
                levelCounter++;
                loadLevel("level" + levelCounter + ".json")
            }, 3000);
        }
        else {
            // randomize error messages
            let rng = Math.floor(Math.random() * (ERROR_MESSAGES.length - 1));
            document.getElementById("messages").innerHTML = ERROR_MESSAGES[rng];
        }
    });

    container.appendChild(button);

    for(let i = 0; i < 5; i++) {
        let switchEl = createSwitch(185 + (i * 108), 230);
        container.appendChild(switchEl);
    }

    document.getElementById("morse").appendChild(container);
}

function createSwitch(x, y) {
    let switchEl = document.createElement("div");
    switchEl.style.position = "absolute";
    switchEl.classList.add("switch");
    switchEl.style.top = y + "px";
    switchEl.style.left = x + "px";
    switchEl.style.width = "70px";
    switchEl.style.height = "140px";
    switchEl.style.backgroundColor = "red";
    switchEl.dataset.switchValue = "0";

    switchEl.addEventListener("click", function() {
        changeSwitch(this);
    });

    return switchEl;
}

function createChoices(choices) {
    let buttonContainer = document.createElement("div");
    buttonContainer.classList.add("buttonContainer");

    for(let choice in choices) {
        let button = document.createElement("button");
        button.innerHTML = choices[choice].action;
        button.value = choice;
        button.dataset.reaction = choices[choice].reaction;

        button.addEventListener("click", function() {
            document.getElementById("morse").innerHTML = this.dataset.reaction;
            checkSolution(this.value, 10000);
        });

        buttonContainer.appendChild(button);
    }

    document.getElementById("controls").appendChild(buttonContainer);
}

function changeSwitch(el) {
    if(el.style.backgroundColor == "red") {
        el.style.backgroundColor = "green";
    }
    else if(el.style.backgroundColor == "green") {
        el.style.backgroundColor = "red";
    }
}

function checkSwitchSolution() {
    let switches = document.getElementsByClassName("switch");
    let solution = "";

    for(let switchEl of switches) {
        if(switchEl.style.backgroundColor == "red") {
            solution += "0"
        }
        else {
            solution += "1";
        }
    }

    return solution == document.getElementById("eventSolution").value;
}

function createNumberInputs() {
    let form = document.createElement("form");
    let numberInput = document.createElement("input");
    numberInput.type = "number";
    numberInput.id = "keypad";

    numberInput.addEventListener("keydown", function(e) {
        if(e.keyCode == 13) {
            e.preventDefault();
            checkSolution(this.value);
        }
    });

    form.appendChild(numberInput);

    let sendButton = document.createElement("input");
    sendButton.type = "button";
    sendButton.value = "send";
    sendButton.addEventListener("click", function() {
        checkSolution(numberInput.value);
    }, false);

    form.appendChild(sendButton);
    document.getElementById("controls").appendChild(form);
}

