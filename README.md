Escape A Boat
===============

Created while attending the [Umma H�sla 2018 Hackathon](https://uh18.diin.io/) by Felix Salcher and Marc Menghin. The motto of the hackathon was "Ship It". It is a two-player escape-the-room game where one player is the captain, which is locked in his room, and the other player is the rescue person trying to get to the captains room. They both need to communicate to solve variouse riddles to move on.

The Captain sits in front of a terminal and is able to gather information about passengers, variouse logs and other vital things. This terminal is implemented as a .Net Core Console application.

The rescue person is moving around the ship in a browser by looking at images of his surrounding and clicking on everything that seems interresting. It's in the style of a point-and-click adventure implemented using HTML and JavaScript.